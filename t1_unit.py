import sqlite3
from datetime import datetime
import smtplib
import unittest
from re import *


# Задание 1
# 1)Используя модуль sqlite3 и модуль smtplib,
# реализуйте реальное добавление пользователей в базу.

# 2)Должны быть реализовать следующие функции и классы:
# - класс пользователя, содержащий в себе следующие методы:
# get_full_name (ФИО с разделением через пробел: «Петров Игорь Сергеевич»),
# get_short_name (ФИО формата: «Петров И. С.»),
# get_age (возвращает возраст пользователя, используя поле birthday типа datetime.date);

# 3)метод __str__ (возвращает ФИО и дату рождения).
# - функция регистрации нового пользователя
# (принимаем экземпляр нового пользователя и отправляем

# 4)Email на почту пользователя с благодарственным письмом).
# - функция отправки Email с благодарственным письмом.
# - функция поиска пользователей в таблице users по имени, фамилии и почте.

# 5)Протестировать данный функционал, используя заглушки, в местах отправки почты.
# При штатном запуске программы, она должна отправлять сообщение на ваш реальный почтовый ящик
# (необходимо настроить SMTP, используя доступы от провайдера вашего Email-сервиса).
# Пример SMTP для сервиса Yandex: https://yandex.ru/support/mail/mail-clients.html#imap
# conn = sqlite3.connect('db.sqlite3')
# conn.execute('CREATE TABLE "users" (id, first_name, last_name, middle_name, birthday)')
# conn.execute('SELECT * FROM "users"')
# conn.execute(
#     """INSERT INTO users(id, first_name, last_name, middle_name, birthday)
#        VALUES (1, "Igor", "Petrov", "Sergeevich", "09-11-1992")
#     """
# )
# print("Показать содеримое базы")
# users = conn.execute('SELECT * FROM "users"').fetchall()
# print(users)

# Задание 1
# Используя модуль sqlite3 и модуль smtplib, реализуйте реальное добавление пользователей в
# базу. Должны быть реализовать следующие функции и классы:
# - класс пользователя, содержащий в себе следующие методы:
# get_full_name (ФИО с разделением через пробел: «Петров Игорь Сергеевич»),
# get_short_name (ФИО формата: «Петров И. С.»),
# get_age (возвращает возраст пользователя, используя поле birthday типа datetime.date); метод
# __str__ (возвращает ФИО и дату рождения).
# - функция регистрации нового пользователя (принимаем экземпляр нового пользователя и
# отправляем Email на почту пользователя с благодарственным письмом).
# - функция отправки Email с благодарственным письмом.
# - функция поиска пользователей в таблице users по имени, фамилии и почте.
# Протестировать данный функционал, используя заглушки, в местах отправки почты. При штатном
# запуске программы, она должна отправлять сообщение на ваш реальный почтовый ящик
# (необходимо настроить SMTP, используя доступы от провайдера вашего Email-сервиса).
# Пример SMTP для сервиса Yandex: https://yandex.ru/support/mail/mail-clients.html#imap


class User:
    def __init__(self, name, last_name, middle_name, y, m, d):
        self.name = name
        self.last_name = last_name
        self.middle_name = middle_name
        self.age = datetime.today().year - y
        self.year = y
        self.month = m
        self.day = d

        # self.name = conn.execute('SELECT first_name FROM users').fetchall()
        # self.last_name = conn.execute('SELECT last_name FROM users').fetchall()
        # self.middle_name = conn.execute('SELECT middle_name FROM users').fetchall()
        # self.age = conn.execute('SELECT birthday FROM users').fetchall()

    def get_full_name(self):
        return f"{self.name} {self.last_name} {self.middle_name}"

    def get_short_name(self):
        return f"{self.last_name} {self.name[0].upper()}. {self.middle_name[0].upper()}."

    def get_age(self):
        return f"Birthday: {self.age}"

    def __str__(self):
        return f"{self.name} {self.last_name} {self.middle_name} {self.year}.{self.month}.{self.day}"


user = User('Adrey', 'Bukin', 'Andreev', 1995, 7, 10)
print(user)
print(user.get_full_name())
print(user.get_short_name())
print(user.get_age())

class UnitTestUser(unittest.TestCase):
    def __init__(self, login, password):
        self.pattern_login = compile('(^|\s)[-a-z0-9_.]+@([-a-z0-9]+\.)+[a-z]{2,6}(\s|$)')
        self.login = login  # youmail@mail.ru

        self.pattern_pwd = compile(r'^(?=.*[0-9].*)(?=.*[a-z].*)[0-9a-zA-Z]{8,}$')
        self.password = password  # a123456

    def unit_text_login(self):
        self.assertRegex(self.login, self.pattern_login, "Bad Login")

    def unit_test_pwd(self):
        self.assertRegex(self.password, self.pattern_pwd, "Bad Password")


gmail_user = 'you@gmail.com'
gmail_password = 'you_password'
to = 'test@gmail.com'
email_text = 'Hello Gmail'

user = UnitTestUser(gmail_user, gmail_password)
user.unit_text_login()
user.unit_test_pwd()

try:
    smtpserver = smtplib.SMTP("smtp.gmail.com", 465)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.ehlo()
    smtpserver.login(gmail_user, gmail_password)
    sent_from = gmail_user
    smtpserver.sendmail(sent_from, to, email_text)
    smtpserver.close()
    print('Email sent!')
except Exception as e:
    print('Something went wrong...')
    print(e)
